package ru.matveev.alexey.atlassian.cloud.listener;


import com.atlassian.connect.spring.AddonUninstalledEvent;
import com.atlassian.connect.spring.AtlassianHostRestClients;
import ru.matveev.alexey.atlassian.cloud.util.YAMLConfig;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class AddonUninstalledListener implements ApplicationListener<AddonUninstalledEvent> {
    @Autowired
    private YAMLConfig yamlConfig;
    @Autowired
    private AtlassianHostRestClients atlassianHostRestClients;

    @Override
    public void onApplicationEvent(AddonUninstalledEvent addonUninstalledEvent) {

        log.info("uninstall event");
        JSONArray endpoints = new JSONArray();
        endpoints.put(String.format("%s/migration-started" , yamlConfig.getBaseUrl()));
        JSONObject webhook = new JSONObject();
        webhook.put("endpoints", endpoints);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(webhook.toString(), headers);
        atlassianHostRestClients
                .authenticatedAsAddon()
                .postForEntity(String.format("%s/rest/atlassian-connect/1/migration/webhook/delete", addonUninstalledEvent.getHost().getBaseUrl()), request, String.class);


    }
}
