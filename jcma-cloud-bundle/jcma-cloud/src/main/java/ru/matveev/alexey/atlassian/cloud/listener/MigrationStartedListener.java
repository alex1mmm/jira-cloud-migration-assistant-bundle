package ru.matveev.alexey.atlassian.cloud.listener;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import ru.matveev.alexey.atlassian.cloud.migrationwebhook.JiraMigration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@Slf4j
public class MigrationStartedListener {
    @Autowired
    private AtlassianHostRestClients atlassianHostRestClients;

    @PostMapping("/migration-started")
    @ResponseBody
    //@IgnoreJwt
    public String migrationStarted(@RequestBody JiraMigration body) {
        log.info("migration started");
        if ( "APP_DATA_UPLOADED".equals(body.getWebhookEventType())) {
            ResponseEntity<String> response = atlassianHostRestClients.authenticatedAsAddon()
                    .getForEntity(String.format("%s/rest/atlassian-connect/1/migration/data/%s/all", body.getMigrationDetails().getCloudUrl(), body.getTransferId()), String.class);
            log.info(response.getBody());
        }
        return "ok";
    }
}
