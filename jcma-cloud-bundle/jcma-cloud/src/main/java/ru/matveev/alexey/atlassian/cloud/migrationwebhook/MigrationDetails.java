
package ru.matveev.alexey.atlassian.cloud.migrationwebhook;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "migrationId",
    "migrationScopeId",
    "name",
    "createdAt",
    "creator",
    "jiraClientKey",
    "confluenceClientKey",
    "cloudUrl",
    "containers"
})
@Data
public class MigrationDetails {

    @JsonProperty("migrationId")
    public String migrationId;
    @JsonProperty("migrationScopeId")
    public String migrationScopeId;
    @JsonProperty("name")
    public String name;
    @JsonProperty("createdAt")
    public Integer createdAt;
    @JsonProperty("creator")
    public String creator;
    @JsonProperty("jiraClientKey")
    public String jiraClientKey;
    @JsonProperty("confluenceClientKey")
    public String confluenceClientKey;
    @JsonProperty("cloudUrl")
    public String cloudUrl;
    @JsonProperty("containers")
    public List<Container> containers = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
