
package ru.matveev.alexey.atlassian.cloud.migrationwebhook;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "cloudAppKey",
    "planName",
    "transferId",
    "migrationDetails",
    "webhookEventType"
})
@Data
public class JiraMigration {

    @JsonProperty("cloudAppKey")
    public String cloudAppKey;
    @JsonProperty("planName")
    public String planName;
    @JsonProperty("transferId")
    public String transferId;
    @JsonProperty("migrationDetails")
    public MigrationDetails migrationDetails;
    @JsonProperty("webhookEventType")
    public String webhookEventType;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
