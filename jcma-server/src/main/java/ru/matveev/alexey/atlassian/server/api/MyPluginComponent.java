package ru.matveev.alexey.atlassian.server.api;

public interface MyPluginComponent
{
    String getName();
}