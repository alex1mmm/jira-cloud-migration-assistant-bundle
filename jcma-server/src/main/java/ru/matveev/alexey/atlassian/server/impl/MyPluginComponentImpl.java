package ru.matveev.alexey.atlassian.server.impl;

import com.atlassian.migration.app.tracker.*;
import ru.matveev.alexey.atlassian.server.accessor.LocalCloudMigrationAccessor;
import ru.matveev.alexey.atlassian.server.api.MyPluginComponent;
import lombok.extern.slf4j.Slf4j;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Named ("myPluginComponent")
public class MyPluginComponentImpl implements CloudMigrationListener, InitializingBean, DisposableBean {

    private final CloudMigrationAccessor accessor;

    @Inject
    public MyPluginComponentImpl(LocalCloudMigrationAccessor accessor) {
        // It is not safe to save a direct reference to the gateway as that can change over time
        this.accessor = accessor.getCloudMigrationAccessor();
    }

    @Override
    public void onRegistrationAccepted() {
        log.info("Nice! The migration listener is ready to take migrations events");
    }

    /**
     * Just a collection of example operations that you can run as part of a migration. None of them are actually required
     */
    @Override
    public void onStartAppMigration(String transferId, MigrationDetails migrationDetails) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            log.info("Migration context summary: " + objectMapper.writeValueAsString(migrationDetails));
            PaginatedMapping paginatedMapping = accessor.getCloudMigrationGateway().getPaginatedMapping(transferId, "identity:user", 5);
            while (paginatedMapping.next()) {

                Map<String, String> mappings = paginatedMapping.getMapping();
                log.info("mappings = {}", objectMapper.writeValueAsString(mappings));
            }
        } catch (IOException e) {
            log.error("Error while running app migration", e);
        }

        // You can also upload one or more files to the cloud. You'll be able to retrieve them through Atlassian Connect
        try {
            OutputStream firstDataStream = accessor.getCloudMigrationGateway().createAppData(transferId);
            // You can even upload big files in here
            firstDataStream.write("Your binary data goes here".getBytes());
            firstDataStream.close();

            // You can also apply labels to distinguish files or to add meta data to support your import process
            OutputStream secondDataStream = accessor.getCloudMigrationGateway().createAppData(transferId, "some-optional-label");
            secondDataStream.write("more bytes".getBytes());
            secondDataStream.close();
        } catch (IOException e) {
            log.error("Error uploading files to the cloud", e);
        }
    }

    @Override
    public void onRegistrarRemoved() {
        log.info("The listener is no longer active");
    }

    @Override
    public String getCloudAppKey() {
        return "ru.matveev.alexey.atlassian.server.jcma-cloud";
    }

    @Override
    public String getServerAppKey() {
        return "ru.matveev.alexey.atlassian.server.jcma-server";
    }

    @Override
    public Set<AccessScope> getDataAccessScopes() {
        return Stream.of(
                AccessScope.APP_DATA_OTHER,
                AccessScope.PRODUCT_DATA_OTHER,
                AccessScope.MIGRATION_TRACING_IDENTITY)
                .collect(Collectors.toCollection(HashSet::new));
    }

    @Override
    public void afterPropertiesSet() {
        this.accessor.registerListener(this);
    }

    @Override
    public void destroy() {
        this.accessor.deregisterListener(this);
    }
}
